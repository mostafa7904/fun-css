window.sortB = function () {
  const start = performance.now();
  var arr = [1, 23, 535, 1, 65, 5, 52];
  var swapped;
  do {
    swapped = false;
    for (var i = 0; i < arr.length - 1; i++) {
      if (arr[i] > arr[i + 1]) {
        var temp;
        temp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = temp;
        swapped = true;
      }
    }
  } while (swapped);
  var end = performance.now();
  var sorted = document.querySelector("#sorted");
  document.querySelector("#sort-time").innerHTML = (end - start).toFixed(4);
  sorted.innerHTML = arr;
};
window.sortR = function () {
  var start = performance.now();
  var arr = [1, 23, 535, 1, 65, 5, 52];
  var idx1, idx2, idx3, len1, len2, radix, radixKey;
  var radices = {},
    buckets = {},
    num,
    curr;
  var currLen, radixStr, currBucket;

  len1 = arr.length;
  len2 = 10;

  for (idx1 = 0; idx1 < len1; idx1++) {
    radices[arr[idx1].toString().length] = 0;
  }
  for (radix in radices) {
    len1 = arr.length;
    for (idx1 = 0; idx1 < len1; idx1++) {
      curr = arr[idx1];
      currLen = curr.toString().length;
      if (currLen >= radix) {
        radixKey = curr.toString()[currLen - radix];
        if (!buckets.hasOwnProperty(radixKey)) {
          buckets[radixKey] = [];
        }
        buckets[radixKey].push(curr);
      } else {
        if (!buckets.hasOwnProperty("0")) {
          buckets["0"] = [];
        }
        buckets["0"].push(curr);
      }
    }
    idx1 = 0;
    for (idx2 = 0; idx2 < len2; idx2++) {
      if (buckets[idx2] != null) {
        currBucket = buckets[idx2];
        len1 = currBucket.length;
        for (idx3 = 0; idx3 < len1; idx3++) {
          arr[idx1++] = currBucket[idx3];
        }
      }
    }
    buckets = {};
  }
  var end = performance.now();
  var sorted = document.querySelector("#sorted");
  document.querySelector("#sort-time").innerHTML = (end - start).toFixed(4);
  sorted.innerHTML = arr;
};

window.reset = function () {
  document.querySelector("#sort-time").innerHTML = "#";
  document.querySelector("#sorted").innerHTML = "";
};

window.sortQ = function () {
  var start = performance.now();

  var items = [1, 23, 535, 1, 65, 5, 52];
  function swap(items, leftIndex, rightIndex) {
    var temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
  }
  function partition(items, left, right) {
    var pivot = items[Math.floor((right + left) / 2)], //middle element
      i = left, //left pointer
      j = right; //right pointer
    while (i <= j) {
      while (items[i] < pivot) {
        i++;
      }
      while (items[j] > pivot) {
        j--;
      }
      if (i <= j) {
        swap(items, i, j); //sawpping two elements
        i++;
        j--;
      }
    }
    return i;
  }

  function quickSort(items, left, right) {
    var index;
    if (items.length > 1) {
      index = partition(items, left, right); //index returned from partition
      if (left < index - 1) {
        //more elements on the left side of the pivot
        quickSort(items, left, index - 1);
      }
      if (index < right) {
        //more elements on the right side of the pivot
        quickSort(items, index, right);
      }
    }
    return items;
  }
  var sortedArray = quickSort(items, 0, items.length - 1);
  var end = performance.now();
  var sorted = document.querySelector("#sorted");
  document.querySelector("#sort-time").innerHTML = (end - start).toFixed(4);
  sorted.innerHTML = sortedArray;
};

window.sortN = function () {
  var start = performance.now();
  var arr = [1, 23, 535, 1, 65, 5, 52];
  arr = arr.sort((a, b) => a - b);
  var end = performance.now();
  var sorted = document.querySelector("#sorted");
  document.querySelector("#sort-time").innerHTML = (end - start).toFixed(4);
  sorted.innerHTML = arr;
};

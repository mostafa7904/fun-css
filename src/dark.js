window.dark = function () {
  var body = document.querySelector("body");
  var darkLogo = document.querySelector("#dark-logo");
  var classList = body.classList;
  if (classList.contains("dark")) {
    classList.remove("dark");
    darkLogo.innerHTML = `<svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 32 32"
      fill="#FFFFFF"
      width="30"
      height="30"
    >
      <title>asleep</title>
      <path
        d="M13.5025,5.4136A15.0755,15.0755,0,0,0,25.096,23.6082a11.1134,11.1134,0,0,1-7.9749,3.3893c-.1385,0-.2782.0051-.4178,0A11.0944,11.0944,0,0,1,13.5025,5.4136M14.98,3a1.0024,1.0024,0,0,0-.1746.0156A13.0959,13.0959,0,0,0,16.63,28.9973c.1641.006.3282,0,.4909,0a13.0724,13.0724,0,0,0,10.702-5.5556,1.0094,1.0094,0,0,0-.7833-1.5644A13.08,13.08,0,0,1,15.8892,4.38,1.0149,1.0149,0,0,0,14.98,3Z"
      />
      <rect
        width="32"
        height="32"
        fill="none"
        data-name="&lt;Transparent Rectangle>"
      />
    </svg>`;
  } else {
    classList.add("dark");
    darkLogo.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" fill="#ffffff" width="32" height="32">
      <title>sunny</title>
      <path d="M16,12a4,4,0,1,1-4,4,4.0045,4.0045,0,0,1,4-4m0-2a6,6,0,1,0,6,6,6,6,0,0,0-6-6Z" transform="translate(0 .005)"/>
      <rect width="2" height="4.958" x="6.854" y="5.375" transform="rotate(-45 7.86 7.856)"/>
      <rect width="5" height="2" x="2" y="15.005"/>
      <rect width="4.958" height="2" x="5.375" y="23.147" transform="rotate(-45 7.86 24.149)"/>
      <rect width="2" height="5" x="15" y="25.005"/>
      <rect width="2" height="4.958" x="23.147" y="21.668" transform="rotate(-45 24.152 24.149)"/>
      <rect width="5" height="2" x="25" y="15.005"/>
      <rect width="4.958" height="2" x="21.668" y="6.854" transform="rotate(-45 24.152 7.856)"/>
      <rect width="2" height="5" x="15" y="2.005"/>
      <rect width="32" height="32" fill="none" data-name="&lt;Transparent Rectangle>"/>
    </svg>`;
  }
};

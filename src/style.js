window.justify = function (type) {
  var wrapper = document.querySelectorAll(".example-wrapper")[0];
  wrapper.classList.forEach((item) => {
    if (item.includes("justify")) {
      wrapper.classList.remove(item);
    }
  });
  wrapper.classList.add("justify-" + type);
};
window.align = function (type) {
  var wrapper = document.querySelectorAll(".example-wrapper")[1];
  wrapper.classList.forEach((item) => {
    if (item.includes("align")) {
      wrapper.classList.remove(item);
    }
  });
  wrapper.classList.add("align-" + type);
};
window.alignText = function (type) {
  var wrapper = document.querySelector(".example-text");
  wrapper.classList.forEach((item) => {
    if (item.includes("text") && item !== "example-text") {
      wrapper.classList.remove(item);
    }
  });
  wrapper.classList.add("text-" + type);
};
